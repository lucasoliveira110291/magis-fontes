﻿using Magis.Domain.Repositorios;
using Magis.Domain.Modulos.Autenticacao;
using System;
using System.Collections.Generic;
using System.Net.Http;
using Unity;
using Unity.Injection;
using Unity.Lifetime;
using Magis.Domain.Modulos.Plataforma;
using Magis.Infraestrutura.Caching;
using Magis.Domain.Autenticacao;
using Magis.Domain.Services;

namespace Magis.Domain.DependencyInjection
{
    public static class ServiceLocator
    {
        public static IUnityContainer Container
        {
            get
            {
                if (_container == null)
                {
                    Inicializar();
                }
                return _container;
            }
        }
        static IUnityContainer _container;

        public static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }

        public static IEnumerable<T> ResolveAll<T>()
        {
            return Container.ResolveAll<T>();
        }

        public static T Resolve<T>(string name)
        {
            try
            {
                return Container.Resolve<T>(name);
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public static void Inicializar()
        {
            IUnityContainer container = new UnityContainer();

            InicializarDependenciasComum(container);
            
            _container = container;
        }

        public static void InicializarApp()
        {
            IUnityContainer container = new UnityContainer();

            InicializarDependenciasComum(container);            

            _container = container;
        }

        public static void InicializarDependenciasComum(IUnityContainer container)
        {
            // REPOSITÓRIOS            
            container.RegisterType<IDbContext, Repositorios.MSSQL.MSSqlDbContext>(new ContainerControlledLifetimeManager(), new InjectionConstructor(ConnectionStringSettings.MSSQL));

            // HTTPCLIENT
            container.RegisterType<HttpClient>(
                new ContainerControlledLifetimeManager(),
                new InjectionFactory(c =>
                {
                    var httpClient = new HttpClient
                    {
                        Timeout = TimeSpan.FromSeconds(30) // 30 segundos
                    };
                    httpClient.DefaultRequestHeaders.ConnectionClose = false;
                    return httpClient;
                }));            
            
            // PLUGINS
            container.RegisterType<IAutenticacao<UsuarioSessaoDTO>, AutenticacaoUsuarioAdmin>(new TransientLifetimeManager());

            // INFRAESTRUTURA
            container.RegisterType<ICaching, MemoriaCache>(new TransientLifetimeManager());

            container.RegisterType<IUsuariosService, UsuariosService>(new TransientLifetimeManager());
        }
    }
}