﻿using Magis.Domain.Repositorios;
using Newtonsoft.Json;
using System;

namespace Magis.Domain.Modulos.Plataforma
{
    public class Usuario : Entidade
    {
        public bool Ativo { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public int IdPerfil { get; set; }
        [JsonIgnore] public string Senha { get; set; }
        [JsonIgnore] public string ImagemUrl { get; set; }
        public bool Administrador { get; set; }
        public bool Desenvolvedor { get; set; }
        public bool Master { get; set; }

        public Usuario()
        {
            CriadoEm = DateTime.Now;
            ImagemUrl = string.Empty;
        }
    }

    public static class UsuarioExtensions
    {
        public static bool VerificarSenhaValida(this Usuario usuario, string senha)
        {
            return usuario.Senha == senha.Criptografar();
        }
    }
}
