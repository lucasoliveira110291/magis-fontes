﻿using Leanwork.CodePack;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Magis.Domain.Modulos.Plataforma.Constantes
{
    public class TipoAmbiente
    {
        public const string PRODUCAO = "PRODUÇÃO";
        public const string HOMOLOGACAO = "HOMOLOGAÇÃO";

        static Dictionary<string, string> _todos;
        public static Dictionary<string, string> Todos
        {
            get
            {
                if (_todos == null)
                {
                    _todos = new Dictionary<string, string>();
                    _todos.Add(PRODUCAO, "PRODUÇÃO");
                    _todos.Add(HOMOLOGACAO, "HOMOLOGAÇÃO");
                }
                return _todos;
            }
        }

        public static string ObterDescricao(string key)
        {
            if (key.IsNullOrWhiteSpace())
            {
                return String.Empty;
            }

            var valor = Todos.FirstOrDefault(x => x.Key == key);

            return valor.Value;
        }
    }
}
