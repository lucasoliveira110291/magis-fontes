﻿namespace Magis.Domain.Modulos.Plataforma
{
    public class UsuarioSessaoDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string ImagemUrl { get; set; }
        public string Email { get; set; }
        public string IdPerfil { get; set; }
        public bool Admin { get; set; }

        public override string ToString() => this.Nome;

        public bool IsPowerUser() =>  Admin;
    }
}
