﻿namespace Magis.Infraestrutura.Caching
{
    public interface ICaching
    {
        T Obter<T>(string chave);
        void Adicionar(string chave, object dados, int duracao);
        bool EstaEmCache(string chave);
        void Limpar(string chave, bool externo = true);
        void LimparPorPattern(string pattern, bool externo = true);
        void Limpar(bool externo = true);
    }
}
