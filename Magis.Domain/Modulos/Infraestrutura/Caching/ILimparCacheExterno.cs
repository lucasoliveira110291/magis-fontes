﻿namespace Magis.Infraestrutura.Caching
{
    public interface ILimparCacheExterno
    {
        void Limpar(string chave);
    }
}
