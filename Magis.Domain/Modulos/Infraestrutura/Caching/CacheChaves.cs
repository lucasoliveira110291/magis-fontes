﻿namespace Magis.Infraestrutura.Caching
{
    public static class CacheChaves
    {
        public static string CONFIGURACOES_TODOS => "CONFIGURACOES_TODOS";
    }

    public static class CacheTempo
    {
        public static int CONFIGURACOES_TODOS => 60;
    }
}
