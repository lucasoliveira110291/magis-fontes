﻿using Magis.Domain.DependencyInjection;
using System;
using System.Linq;
using System.Runtime.Caching;

namespace Magis.Infraestrutura.Caching
{
    public class MemoriaCache : ICaching
    {
        public ILimparCacheExterno LimparCacheExterno
        {
            get { return _limparCacheExterno ?? (_limparCacheExterno = ServiceLocator.Resolve<ILimparCacheExterno>()); }
            set { _limparCacheExterno = value; }
        }
        ILimparCacheExterno _limparCacheExterno;

        protected ObjectCache Cache
        {
            get { return MemoryCache.Default; }
        }

        public T Obter<T>(string chave)
        {
            var objetoCacheado = Cache[chave];
            return (T)objetoCacheado;
        }

        public void Adicionar(string chave, object dados, int duracao)
        {
            if (dados == null)
            {
                return;
            }

            var politica = new CacheItemPolicy
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(duracao)
            };
            Cache.Add(new CacheItem(chave, dados), politica);
        }

        public bool EstaEmCache(string chave)
        {
            return (Cache.Contains(chave));
        }

        public void Limpar(string chave, bool externo = true)
        {
            Cache.Remove(chave);
            if (externo)
            {
                LimparCacheExterno.Limpar(chave);
            }
        }

        public void LimparPorPattern(string pattern, bool externo = true)
        {
            var chavesRemover = Cache
                .Where(x => x.Key.Contains(pattern))
                .Select(x => x.Key);
            foreach (var chave in chavesRemover)
            {
                Limpar(chave, externo);
            }
        }

        public void Limpar(bool externo = true)
        {
            foreach (var item in Cache)
            {
                Limpar(item.Key);
                if (externo)
                {
                    LimparCacheExterno.Limpar(item.Key);
                }                
            }
        }
    }
}
