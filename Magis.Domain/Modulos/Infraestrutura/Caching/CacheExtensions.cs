﻿using System;
using System.Runtime.Serialization;

namespace Magis.Infraestrutura.Caching
{
    public static class CacheExtensions
    {
        public static T Obter<T>(this ICaching caching, 
            string chave, 
            int duracaoPadrao, 
            Func<T> executar,
            Func<T, int> duracao = null)
        {
            try
            {
                if (!CacheAtivo() || duracaoPadrao <= 0)
                {
                    return executar();
                }

                if (caching.EstaEmCache(chave))
                {
                    return caching.Obter<T>(chave);
                }

                return ExecutarGuardar(caching, chave, duracaoPadrao, executar, duracao);
            }
            catch (SerializationException)
            {
                caching.Limpar(chave);
                return ExecutarGuardar(caching, chave, duracaoPadrao, executar, duracao);
            }
            catch (Exception)
            {
#if !DEBUG
                return executar(); 
#else
                throw;
#endif
            }
        }

        private static T ExecutarGuardar<T>(ICaching caching, 
            string chave, 
            int duracaoPadrao, 
            Func<T> executar,
            Func<T, int> duracao)
        {
            var resultado = executar();

            var cacheTempo = 0;
            if (duracao != null)
            {
                cacheTempo = duracao(resultado);
                if (cacheTempo > 0)
                {
                    duracaoPadrao = cacheTempo;
                }
            }

            caching.Adicionar(chave, resultado, duracaoPadrao);

            return resultado;
        }

        private static bool CacheAtivo()
        {
#if DEBUG
            return false; // cache desabilitado em modo debug
#else
             // se não existir chave de configuração de cache, o cache estará sempre habilitado.
            bool.TryParse(WebConfigurationManager.AppSettings.Get("CACHE") ?? System.Boolean.TrueString, out bool cacheAtivo);
            return cacheAtivo;
#endif
        }
    }
}
