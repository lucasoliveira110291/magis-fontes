﻿using Magis.Domain.Autenticacao;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.Security;

namespace Magis.Domain.Modulos.Autenticacao
{
    public abstract class AutenticacaoForms<T> : IAutenticacao<T>
    {
        static HttpContext HttpContext
        {
            get { return HttpContext.Current; }
        }

        protected abstract string NomeCookie { get; }

        public void Autenticar(T dados, bool manterConectado = false)
        {
            Deslogar();

            var hoje = DateTime.Now;
            var expiraEm = hoje.Add(FormsAuthentication.Timeout);
            string dadosUsuario = JsonConvert.SerializeObject(dados);

            var ticket = new FormsAuthenticationTicket(1, dados.ToString(), hoje, expiraEm, false, dadosUsuario, FormsAuthentication.FormsCookiePath);

            var cookieAutenticado = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket))
            {
                HttpOnly = false,
                Secure = FormsAuthentication.RequireSSL,
                Path = FormsAuthentication.FormsCookiePath,
            };

            if (manterConectado)
            {
                cookieAutenticado.Expires = hoje.AddYears(1);
            }

            HttpContext.Response.Cookies.Remove(NomeCookie);
            HttpContext.Response.Cookies.Add(cookieAutenticado);            
        }

        public void Deslogar()
        {
            HttpCookie cookie = HttpContext.Request.Cookies[this.NomeCookie];
            if (cookie != null)
            {
                cookie.Value = null;
                cookie.Expires = DateTime.Now.AddMonths(-1);
                cookie.Secure = FormsAuthentication.RequireSSL;
                cookie.Path = FormsAuthentication.FormsCookiePath;
                cookie.Domain = FormsAuthentication.CookieDomain;
                HttpContext.Response.Cookies.Remove(this.NomeCookie);
                HttpContext.Response.Cookies.Add(cookie);
            }
            FormsAuthentication.SignOut();
        }

        public T ObterAutenticado()
        {
            T dados = default(T);
            
            if (HttpContext == null ||
                HttpContext.User == null ||
                HttpContext.User.Identity == null)
            {
                return dados;
            }

            FormsIdentity identidade = HttpContext.User.Identity as FormsIdentity;
            if (identidade == null)
            {
                return dados;
            }

            FormsAuthenticationTicket ticket = identidade.Ticket;
            if (ticket == null)
            {
                return dados;
            }

            try
            {
                dados = JsonConvert.DeserializeObject<T>(ticket.UserData);
            }
            catch (Exception)
            {
                return dados;
            }

            return dados;
        }

        public bool EstaAutenticado()
        {
            return HttpContext.User.Identity.IsAuthenticated;
        }
    }
}