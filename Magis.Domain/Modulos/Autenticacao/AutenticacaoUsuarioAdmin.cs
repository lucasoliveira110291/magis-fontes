﻿using Magis.Domain.Modulos.Plataforma;

namespace Magis.Domain.Modulos.Autenticacao
{
    public class AutenticacaoUsuarioAdmin : AutenticacaoForms<UsuarioSessaoDTO>
    {
        protected override string NomeCookie
        {
            get { return "leancommerce_gestao_admin_auth"; }
        }
    }
}