﻿namespace Magis.Domain.Autenticacao
{
    public interface IAutenticacao<T>
    {
        void Autenticar(T dados, bool manterConectado = false);
        void Deslogar();
        T ObterAutenticado();
        bool EstaAutenticado();
    }
}
