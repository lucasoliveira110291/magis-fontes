﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace Magis.Domain.Modulos.Comum
{
    public interface IGerenciarUltimoAcesso
    {
        void Gravar(string url);
        string Recuperar();
    }

    public class GerenciarUltimoAcessoCookie : IGerenciarUltimoAcesso
    {
        const string ULTIMOACESSO_COOKIE = "leancommerce_loja_ultimoacesso";

        public void Gravar(string url)
        {
            if (HttpContext.Current == null)
            {
                return;
            }
            if (HttpContext.Current.Response == null)
            {
                return;
            }
            if (HttpContext.Current.Response.Cookies == null)
            {
                return;
            }

            HttpRequest request = HttpContext.Current.Request;
            HttpResponse response = HttpContext.Current.Response;

            var cookie = request.Cookies[ULTIMOACESSO_COOKIE] ?? new HttpCookie(ULTIMOACESSO_COOKIE);
            cookie.Value = request.Url.AbsoluteUri;
            cookie.Expires = DateTime.Now.AddYears(1);
            if (FormsAuthentication.IsEnabled)
            {
                cookie.Domain = FormsAuthentication.CookieDomain;
            }
            response.Cookies.Add(cookie);
        }

        public string Recuperar()
        {
            string url = null;

            if (HttpContext.Current == null)
            {
                return url;
            }
            if (HttpContext.Current.Request == null)
            {
                return url;
            }
            if (HttpContext.Current.Request.Cookies == null)
            {
                return url;
            }

            var cookieCampanha = HttpContext.Current.Request.Cookies[ULTIMOACESSO_COOKIE];
            if (cookieCampanha == null)
            {
                return url;
            }

            return cookieCampanha.Value;
        }
    }
}
