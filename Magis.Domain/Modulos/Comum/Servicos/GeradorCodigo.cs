﻿using System;
using System.Linq;

namespace Magis.Domain.Modulos.Comum
{
    public class GeradorCodigo
    {
        const string ALFABETO = "AG8FOLE2WVT1CPY5ZH3NIUDBXSMQK7946";

        public static string GerarGuid()
        {
            return Guid.NewGuid().ToString();
        }

        public static string Gerar(int lenght = 6)
        {
            Random random = new Random();

            char[] chaves = ALFABETO.ToCharArray();

            return Enumerable
                .Range(1, lenght) // for(i.. ) 
                .Select(k => chaves[random.Next(0, chaves.Length - 1)])  // generate a new random char 
                .Aggregate("", (e, c) => e + c); // join into a string
        }
    }
}
