﻿using System;

namespace Magis.Domain.Modulos.Comum
{
    public class Vigencia
    {
        public DateTime? Inicio { get; set; }
        public DateTime? Fim { get; set; }

        public Vigencia()
        {
        }

        public Vigencia(DateTime? inicio, DateTime? fim)
        {
            Inicio = inicio;
            Fim = fim;
        }

        public bool EstaVigente()
        {
            DateTime now = DateTime.Now.Date;
            return (!Inicio.HasValue || now.Date >= Inicio.Value.Date) && (!Fim.HasValue || now.Date <= Fim.Value.Date);
        }

        public override string ToString()
        {
            if (!Inicio.HasValue && !Fim.HasValue)
            {
                return "INDETERMINADO";
            }
            else if (Inicio.HasValue && !Fim.HasValue)
            {
                return String.Format("A partir de {0:dd/MM/yyyy}", Inicio.Value);
            }
            else if (!Inicio.HasValue && Fim.HasValue)
            {
                return String.Format("Até {0:dd/MM/yyyy}", Fim.Value);
            }

            return String.Format("De {0:dd/MM/yyyy} até {1:dd/MM/yyyy}", Inicio.Value, Fim.Value);
        }
    }
}
