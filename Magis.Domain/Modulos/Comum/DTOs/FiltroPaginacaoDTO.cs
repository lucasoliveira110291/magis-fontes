﻿namespace Magis.Domain.Modulos.Comum
{
    public class FiltroPaginacaoDTO
    {
        public int Pagina
        {
            get { return (_pagina <= 0) ? 1 : _pagina; }
            set { _pagina = value; }
        }
        int _pagina;

        public int Tamanho
        {
            get { return (_tamanho <= 0) ? 12 : _tamanho; }
            set { _tamanho = value; }
        }
        int _tamanho;

        public int OffSet => (Pagina - 1) * Tamanho;
    }
}
