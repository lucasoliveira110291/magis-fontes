﻿namespace Magis.Domain.Modulos.Comum
{
    public class MedidasDTO
    {
        public decimal Peso { get; set; }
        public decimal Altura { get; set; }
        public decimal Largura { get; set; }
        public decimal Profundidade { get; set; }
    }
}
