﻿using System;
using Leanwork.CodePack;

namespace Magis.Domain.Modulos.Comum
{
    public class EnderecoDTO
    {
        public string Nome { get; set; }

        public string Logradouro { get; set; }

        public string Complemento { get; set; }

        public string Numero { get; set; }

        public string Bairro { get; set; }

        public string Cidade { get; set; }

        public string Estado { get; set; }

        public string Pais { get; set; }

        string _codigoPostal;
        public string CEP
        {
            get { return _codigoPostal; }
            set { _codigoPostal = value.RemoveAlphabetic(); }
        }

        public string CodigoPostalFormatado
        {
            get { return CEP.BrazilianPostalCode(); }
        }
    }
}
