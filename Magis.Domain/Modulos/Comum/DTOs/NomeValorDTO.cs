﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magis.Domain.Modulos.Comum
{
    public class NomeValorDTO
    {
        public object Id { get; set; }

        public string Descricao { get; set; }
    }
}
