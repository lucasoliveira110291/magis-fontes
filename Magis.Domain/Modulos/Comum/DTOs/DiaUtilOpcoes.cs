﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magis.Domain.Modulos.Comum
{
    public class DiaUtilOpcoes
    {
        public bool Sabado { get; set; }
        public bool Domingo { get; set; }
        public bool Feriado { get; set; }
    }
}