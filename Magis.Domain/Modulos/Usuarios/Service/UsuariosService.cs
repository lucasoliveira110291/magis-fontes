﻿using Magis.Domain.DependencyInjection;
using Magis.Domain.Modulos.Usuarios.DTOs;
using Magis.Domain.Repositorios;
using Dapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magis.Domain.Services
{
    public interface IUsuariosService
    {
        IEnumerable<UsuarioDTO> ObterTodos();
        Task<UsuarioDTO> AdicionarUsuario(UsuarioDTO usuario);
    }

    public class UsuariosService : IUsuariosService
    {
        public IDbContext DbContext
        {
            get => _dbContext ?? (_dbContext = ServiceLocator.Resolve<IDbContext>());
            set => _dbContext = value;
        }
        IDbContext _dbContext;

        public IEnumerable<UsuarioDTO> ObterTodos()
        {
            var sql = "SELECT Id " +
              ",Cpf " +
              ",Nome " +
              ",Matricula " +
              ",Email " +
              ",Telefone " +
              ",Senha " +
              ",IdPerfil " +
              ",Ativo " +
            "FROM Usuarios";

            using (var multi = DbContext.CreateConnection().QueryMultiple(sql))
            {
                return multi.Read<UsuarioDTO>();
            }
        }

        public async Task<UsuarioDTO> AdicionarUsuario(UsuarioDTO model)
        {
            try
            {
                using (var conexao = DbContext.CreateConnection()) // OK
                {
                    conexao.Open();
                    using (var transacao = conexao.BeginTransaction())
                    {
                        //try
                        //{
                        transacao.Connection.Execute("INSERT INTO Usuarios(Cpf,Nome,Matricula,Email,Telefone,Senha,IdPerfil,Ativo) VALUES (@Cpf,@Nome,@Matricula,@Email,@Telefone,@Senha,@IdPerfil,@Ativo)",
                            param: new
                            {
                                model.Cpf,
                                model.Nome,
                                model.Matricula,
                                model.Email,
                                model.Telefone,
                                model.Senha,
                                model.IdPerfil,
                                model.Ativo
                            }, transaction: transacao);

                        transacao.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                model.AdicionarErro("Erro ao salvar usuário " + ex);
            }

            return model;
        }
    }
}
