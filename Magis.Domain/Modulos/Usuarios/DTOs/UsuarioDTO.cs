﻿using Magis.Models.Comum;

namespace Magis.Domain.Modulos.Usuarios.DTOs
{
    public class UsuarioDTO : ResultadoBase
    {
        public int Id { get; set; }

        public string Cpf { get; set; }

        public string Nome { get; set; }

        public string Matricula { get; set; }

        public string Email { get; set; }

        public string Telefone { get; set; }

        public string Senha { get; set; }

        public bool Ativo { get; set; }

        public int IdPerfil { get; set; }

        public string EditarUrl => "sistema/usuarios/editar/" + this.Id;
        public string AlterarSituacaoUrl => "sistema/usuarios/alterar-situacao/" + this.Id;

    }
}
