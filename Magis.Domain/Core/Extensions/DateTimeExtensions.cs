﻿using Magis.Domain.Modulos.Comum;
using System;

namespace magis.Domain
{
    public static class DateTimeExtensions
    {
        public static DateTime GetDateTimeBrasilian(this DateTime date)
        {
            try
            {
                return TimeZoneInfo.ConvertTime(date, TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time"));
            }
            catch (Exception)
            {
                return date;
            }
        }

        public static DateTime AddWorkDays(this DateTime d, int days, DiaUtilOpcoes opcoes = null)
        {
            int count = 0;

            bool permiteSabado = (opcoes == null ? false : opcoes.Sabado);
            bool permiteDomingo = (opcoes == null ? false : opcoes.Domingo);

            while (count < days)
            {
                d = d.AddDays(1);
                while ((d.DayOfWeek == DayOfWeek.Saturday && !permiteSabado) 
                    || (d.DayOfWeek == DayOfWeek.Sunday && !permiteDomingo))
                {
                    d = d.AddDays(1);
                }
                count++;
            }

            return d;
        }

        public static bool IsWorkday(this DayOfWeek d, DiaUtilOpcoes opcoes = null)
        {
            return  (opcoes?.Sabado == true || d != DayOfWeek.Saturday) && 
                    (opcoes?.Domingo == false || d != DayOfWeek.Sunday);
        }

        public static DateTime UtcToHorarioDeBrasilia(this DateTime d)
        {
            var tnBrasilia = TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time"); // Brasilia/BRA
            var horarioDeBrasilia = TimeZoneInfo.ConvertTimeFromUtc(d, tnBrasilia);
            return horarioDeBrasilia;
        }

        public static DateTime LocalToUtcDate(this DateTime d)
        {
            return d.ToUniversalTime().Date;
        }

        public static DateTime LocalToUtcDateTime(this DateTime d)
        {
            return d.ToUniversalTime();
        }

        public static string Formatar(this DateTime d, string formato = "dd/MM/yyyy HH:mm:ss")
        {
            return d.ToString(formato);
        }

        public static int TotalMeses(this DateTime inicio, DateTime fim)
        {
            return Math.Abs(12 * (inicio.Year - fim.Year) + inicio.Month - fim.Month);            
        }
    }
}
