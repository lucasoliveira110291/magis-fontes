﻿using System;
using System.Globalization;

namespace Magis.Domain
{
    public static class DecimalExtensions
    {
        public static int Porcentagem(this decimal total, decimal valor)
        {
            if (valor <= 0)
            {
                return 0;
            }

            var desconto = (total - valor);
            return (int)((desconto / total) * 100).Arredondar();
        }

        public static decimal Calcular(this decimal valor, decimal porcentagem)
        {
            if (porcentagem <= 0)
            {
                return decimal.Zero;
            }

            return (valor * porcentagem) / 100;
        }

        public static decimal Arredondar(this decimal valor, int casas = 2)
        {
            return Decimal.Round(valor, casas, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Calcula o valor do desconto que deverá ser aplicado a um item em função do seu peso em relação aos total de todos os demais itens.
        /// </summary>
        /// <param name="valorDesconto">Valor do desconto que deverá ser rateado.</param>
        /// <param name="precoTotalItem">Preço total do item.</param>
        /// <param name="precoTotalItens">Preço total de todos os itens.</param>
        /// <returns></returns>
        public static decimal RatearDesconto(this decimal valorDesconto, decimal precoTotalItem, decimal precoTotalItens)
        {
            var rateioPorcentagemDesconto = precoTotalItem / precoTotalItens;
            return valorDesconto * rateioPorcentagemDesconto;
        }

        public static Nullable<decimal> TryDecimalCurrencyNullable(this string incoming)
        {
            Nullable<decimal> returnValue = null;
            decimal result;
            if (decimal.TryParse(incoming, NumberStyles.Currency, CultureInfo.CreateSpecificCulture("pt-BR"), out result))
            {
                returnValue = result;
            }
            return returnValue;
        }

        public static decimal TryDecimalCurrency(this string incoming)
        {   
            decimal result;

            decimal.TryParse(incoming, NumberStyles.Currency, CultureInfo.CreateSpecificCulture("pt-BR"), out result);
            
            return result;
        }
    }
}
