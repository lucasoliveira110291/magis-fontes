﻿using Magis.Domain.Infraestrutura.Seguranca;

namespace Magis.Domain
{
    public static class CryptoExtensions
    {
        public static string Criptografar(this string valor)
        {
            if (string.IsNullOrWhiteSpace(valor))
                return string.Empty;

            return AES256Crypto.Criptografar(valor);
        }

        public static string Descriptografar(this string valor)
        {
            if (string.IsNullOrWhiteSpace(valor))
                return string.Empty;

            return AES256Crypto.Descriptografar(valor);
        }
    }
}
