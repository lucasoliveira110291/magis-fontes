﻿using System.Data;

namespace Magis.Domain
{
    public static class SqlDatabaseExtensions
    {
        public static void SecureRollback(this IDbTransaction transaction)
        {
            if (transaction != null)
            {
                transaction.Rollback();
            }
        }
    }
}
