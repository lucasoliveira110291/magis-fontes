﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace magis.Domain
{
    public static class IntegerExtensions
    {
        public static string EmDiasUteis(this int dias)
        {
            if (dias < 0)
            {
                return "Imediato";
            }
            if (dias == 0)
            {
                return "Em até 24h";
            }
            if (dias == 1)
            {
                return $"{dias} dia útil";
            }

            return $"{dias} dias úteis";
        }

        public static string EmDiasUteis(this int? dias)
        {
            if (dias.HasValue == false)
            {
                return null;
            }
            return dias.Value.EmDiasUteis();
        }

        public static string EmDiasUteis(this short dias)
        {
            return EmDiasUteis((int)dias);
        }
    }
}
