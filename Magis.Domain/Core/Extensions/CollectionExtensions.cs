﻿using System.Collections.Generic;

namespace Magis.Domain
{
    public static class CollectionExtensions
    {
        public static void AddIfNotNull<T>(this ICollection<T> source, T item)
        {
            if (item != null)
                source.Add(item);
        }

        public static void AddIfNotContais<T>(this ICollection<T> source, T item)
        {
            if (item == null) return;
            if (!source.Contains(item))
            {
                source.Add(item);
            }
        }
    }
}
