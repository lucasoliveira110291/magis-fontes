﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Web;

namespace Magis.Domain
{
    public static class JsonExtensions
    {       
        public static string GetJson(this object obj, 
            bool camelCase = false,
            bool javascriptEncode = false)
        {
            if (obj == null)
            {
                obj = new object();
            }

            var settings = new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                Formatting = Formatting.None,
                StringEscapeHandling = StringEscapeHandling.EscapeHtml
            };

            if (camelCase)
            {
                settings.ContractResolver = new CamelCasePropertyNamesContractResolver();    
            }

            string jsonString = JsonConvert.SerializeObject(obj, settings);
            if (javascriptEncode)
            {
                jsonString = HttpUtility.JavaScriptStringEncode(jsonString);
            }

            return jsonString;
        }
    }
}
