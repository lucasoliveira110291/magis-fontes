﻿using System.Data.SqlClient;

namespace Magis.Domain
{
    public static class SqlExceptionExtensions
    {
        public static bool IsDuplicate(this SqlException ex)
        {
            /// See: select * from master.dbo.sysmessages where(error = 2601 or error = 2627)
            return ex.Number == 2601 || ex.Number == 2627;
        }

        public static bool IsDuplicate(this SqlException ex, string index)
        {
            return ex.IsDuplicate() && ex.Message.Contains(index);
        }

        public static bool IsApplicationError(this SqlException ex)
        {
            return ex.Number == 99999;
        }

        public static bool IsForeignKeyViolation(this SqlException ex)
        {
            // See: select text from sys.messages where message_id=547 and language_id=1046
            return ex.Number == 547 || ex.Message.Contains("FOREIGN KEY");
        }

        public static bool IsForeignKeyViolation(this SqlException ex, string message)
        {
            return IsForeignKeyViolation(ex) && ex.Message.Contains(message);
        }
    }
}
