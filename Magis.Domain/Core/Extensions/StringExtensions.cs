﻿using Leanwork.CodePack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Magis.Domain
{
    /// <summary>
    /// String's Extensions 
    /// </summary>
    public static class StringExtensions
    {
        public static string SeForNull(this string valor, string valorPadrao)
        {
            if (String.IsNullOrWhiteSpace(valor))
            {
                return valorPadrao.SafeTrim();
            }
            return valor.SafeTrim();
        }

        public static string RemoverAcentos(this string valor)
        {
            return Leanwork.CodePack.StringExtensions.RemoveAccents(valor);
        }

        public static string ApenasNumeros(this string valor)
        {
            if (string.IsNullOrWhiteSpace(valor))
            {
                return String.Empty;
            }   
            return Regex.Replace(valor.SafeTrim(), @"[^\d]", "");
        }
        
        public static string Maiusculo(this string valor)
        {
            if (String.IsNullOrWhiteSpace(valor))
            {
                return String.Empty;
            }
            return valor.Trim().ToUpperInvariant();
        }

        public static string Minusculo(this string valor)
        {
            if (String.IsNullOrWhiteSpace(valor))
            {
                return String.Empty;
            }
            return valor.Trim().ToLowerInvariant();
        }

        public static string SafeTrim(this string valor)
        {
            if (String.IsNullOrWhiteSpace(valor))
            {
                return String.Empty;
            }
            return valor.Trim();
        }

        public static string SafeToLower(this string valor)
        {
            if (String.IsNullOrWhiteSpace(valor))
            {
                return String.Empty;
            }
            return valor.ToLower();
        }

        public static string SafeReplace(this string valor, string antigoValor, string novoValor)
        {
            if (String.IsNullOrWhiteSpace(valor))
            {
                return String.Empty;
            }
            return valor.Replace(antigoValor, novoValor);
        }

        public static string ToCpf(this string cpf)
        {
            return Regex.Replace(cpf, @"^(\d{3})(\d{3})(\d{3})(\d{2})$", "$1.$2.$3-$4");
        }

        public static string ToCnpj(this string cnpj)
        {
            return Regex.Replace(cnpj, @"^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$", "$1.$2.$3/$4-$5");
        }

        public static Int32 ToInt32(this string value)
        {
            return Leanwork.CodePack.StringExtensions.ToInt32(value);
        }

        public static Int64 ToInt64(this string value)
        {
            return Leanwork.CodePack.StringExtensions.ToInt64(value);
        }

        public static Decimal ToDecimal(this string value)
        {
            return Leanwork.CodePack.StringExtensions.ToDecimal(value);
        }

        //static string[] valuesTrue = new string[] { "1", "SIM", "YES", "OK", "X", "TRUE" };

        public static Boolean ToBoolean(this string value)
        {
            return Leanwork.CodePack.StringExtensions.ToBoolean(value);
        }

        public static bool PossuiExtensao(this string valor, params string[] extensoes)
        {
            if (String.IsNullOrWhiteSpace(valor))
            {
                return false;
            }
            if (extensoes == null || extensoes.Length <= 0)
            {
                return false;
            }
            return extensoes.Any(extensao => valor.EndsWith(extensao));
        }

        public static IEnumerable<string> QuebraPermalink(this string permalink)
        {
            if (String.IsNullOrWhiteSpace(permalink))
            {
                return Enumerable.Empty<string>();
            }

            string caminho = String.Empty;
            var links = permalink.Split(new string[1] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            var retorno = new List<string>(links.Count());
            foreach (var item in links)
            {
                if (String.IsNullOrWhiteSpace(caminho))
                {
                    caminho = item;
                }
                else
                {
                    caminho = String.Concat(caminho, "/", item);
                }

                retorno.Add(caminho);
            }

            return retorno;
        }

        public static string MascaraCartao(this string cartao)
        {
            if (String.IsNullOrWhiteSpace(cartao))
            {
                return null;
            }
            return cartao.Substring(cartao.Length - 4).PadLeft(12, '*');
        }

        /// <summary>
        /// Convert decimal to price BR
        /// </summary>
        /// <param name="value">value</param>
        /// <returns>String value</returns>
        public static string ToPriceBR(this decimal value)
        {
            return ToPriceBR(value.ToString());
        }

        /// <summary>
        /// Convert string to price USA
        /// </summary>
        /// <param name="value">value</param>
        /// <returns>String value</returns>
        public static string ToPriceBR(this string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                return String.Empty;

            return String.Format("{0:0.00}", value).Replace(".", ",");
        }

        public static string[] SecureSplit(this string value, string separator = ",")
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                return new string[0];
            }

            return value.Split(new string[1] { separator }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static bool IsValidEmail(this string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                return false;

            var rg = new Regex(@"^[A-Za-z0-9\._\-]+@[A-Za-z0-9\.\-]+\.([A-Za-z]{2,})$");

            return rg.IsMatch(value);
        }

        public static string ToUTF8(this string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                return String.Empty;

            byte[] bytes = Encoding.Default.GetBytes(value);

            return Encoding.UTF8.GetString(bytes);
        }

        public static string NormalizarTextoBusca(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return string.Empty;

            return Regex.Replace(value.RemoverAcentos(), @"[^a-zA-Z0-9_. ]+", string.Empty);
        }
     
        public static string ToSlug(this string value)
        {
            return Leanwork.CodePack.StringExtensions.ToSlug(value);
        }

    }
}
