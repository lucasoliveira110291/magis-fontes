﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Magis.Domain
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> Embaralhar<T>(this IEnumerable<T> source)
        {
            Random rng = new Random();
            T[] elements = source.ToArray();
            // Note i > 0 to avoid final pointless iteration
            for (int i = elements.Length - 1; i > 0; i--)
            {
                // Swap element "i" with a random earlier element it (or itself)
                int swapIndex = rng.Next(i + 1);
                T tmp = elements[i];
                elements[i] = elements[swapIndex];
                elements[swapIndex] = tmp;
            }
            // Lazily yield (avoiding aliasing issues etc)
            foreach (T element in elements)
            {
                yield return element;
            }
        }

        public static T SelecionarAleatorio<T>(this IEnumerable<T> source)
        {
            return source.SelecionarAleatorio(1).FirstOrDefault();
        }

        public static IEnumerable<T> SelecionarAleatorio<T>(this IEnumerable<T> source, int count)
        {
            return source.Embaralhar().Take(count);
        }

        public static bool SecureAny<T>(this IEnumerable<T> source)
        {
            return (source != null) && source.Any();
        }
        public static bool SecureAny<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            return (source != null) && source.Any(predicate);
        }
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return (source == null) || (!source.Any());
        }

        public static IEnumerable<T> ToEnumerableOrEmpty<T>(this IEnumerable<T> source)
        {
            return source ?? Enumerable.Empty<T>();
        }

        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> source, Func<T, TKey> keySelector)
        {                               
            HashSet<TKey> knownKeys = new HashSet<TKey>();

            foreach (T element in source)
            {
                if (knownKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

        public static IEnumerable<IndexedValue<T>> WithIndex<T>(this IEnumerable<T> source)
        {
            return source.Select((value, index) => new IndexedValue<T>(value, index));
        }

    }

    public struct IndexedValue<T>
    {
        private readonly T value;
        private readonly int index;

        public T Value { get { return value; } }
        public int Index { get { return index; } }

        public IndexedValue(T value, int index)
        {
            this.value = value;
            this.index = index;
        }
    }
}
