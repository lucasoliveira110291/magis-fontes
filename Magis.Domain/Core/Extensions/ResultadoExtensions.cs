﻿using System;

namespace Magis.Domain
{
    public static class ResultadoExtensions
    {
        public static void Unir(this Resultado result, params Resultado[] otherResults)
        {
            if (otherResults != null && otherResults.Length != 0)
            {
                foreach (var item in otherResults)
                {
                    result.AdicionarErros(item.Erros);
                    result.AdicionarAvisos(item.Avisos);
                }
            }
        }

        public static T Clonar<T>(this Resultado result) where T : Resultado
        {
            T newResult = Activator.CreateInstance<T>();
            newResult.AdicionarErros(result.Erros);
            newResult.AdicionarAvisos(result.Avisos);
            return newResult;
        }
    }
}
