﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Magis.Domain
{
    /// <summary>
    /// Pagination strategy
    /// </summary>
    public interface IPaginacao
    {
        int PaginaAtual { get; }
        int TamanhoPaginacao { get; }
        int TotalRegistros { get; }
        int TotalPaginas { get; }
        int Proxima { get; }
        int Anterior { get; }
        bool TemPaginas { get; }
        bool TemProxima { get; }
        bool TemAnterior { get; }
    }

    /// <summary>
    /// Paged list
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    public class Paginacao<T> : List<T>, IPaginacao
    {
        public Paginacao(IQueryable<T> source, int paginaAtual, int tamanhoPaginacao)
        {
            paginaAtual = (paginaAtual >= 0) ? paginaAtual : 0;
            tamanhoPaginacao = (tamanhoPaginacao > 0) ? tamanhoPaginacao : 24;

            int total = source.Count();
            this.TotalRegistros = total;
            this.TotalPaginas = total / tamanhoPaginacao;

            if (total % tamanhoPaginacao > 0)
                TotalPaginas++;

            this.TamanhoPaginacao = tamanhoPaginacao;
            this.PaginaAtual = paginaAtual;

            this.AddRange(source.Skip((paginaAtual - 1) * tamanhoPaginacao).Take(tamanhoPaginacao).ToList());
            
            this.PrimeiraPagina = this.PaginaAtual - 5;
            this.UltimaPagina = this.PaginaAtual + 4;

            if (this.PrimeiraPagina <= 0)
            {
                this.UltimaPagina -= (this.PrimeiraPagina - 1);
                this.PrimeiraPagina = 1;
            }
            if (this.UltimaPagina > this.TotalPaginas)
            {
                this.UltimaPagina = this.TotalPaginas;
                if (this.UltimaPagina > 10)
                {
                    this.PrimeiraPagina = this.UltimaPagina - 9;
                }
            }
        }

        public Paginacao(IList<T> source, int paginaAtual, int tamanhoPaginacao)
        {
            paginaAtual = (paginaAtual > 0) ? paginaAtual : 1;
            tamanhoPaginacao = (tamanhoPaginacao > 0) ? tamanhoPaginacao : 1;

            TotalRegistros = source.Count();
            TotalPaginas = TotalRegistros / tamanhoPaginacao;

            if (TotalRegistros % tamanhoPaginacao > 0)
                TotalPaginas++;

            this.TamanhoPaginacao = tamanhoPaginacao;
            this.PaginaAtual = paginaAtual;
            this.AddRange(source.Skip((paginaAtual - 1) * tamanhoPaginacao).Take(tamanhoPaginacao).ToList());
        }

        public Paginacao(IEnumerable<T> source, int paginaAtual, int tamanhoPaginacao, int totalRegistros)
        {
            paginaAtual = (paginaAtual > 0) ? paginaAtual : 1;
            tamanhoPaginacao = (tamanhoPaginacao > 0) ? tamanhoPaginacao : 1;

            TotalRegistros = totalRegistros;
            TotalPaginas = TotalRegistros / tamanhoPaginacao;

            if (TotalRegistros % tamanhoPaginacao > 0)
                TotalPaginas++;

            this.TamanhoPaginacao = tamanhoPaginacao;
            this.PaginaAtual = paginaAtual;
            this.AddRange(source);

            this.PrimeiraPagina = this.PaginaAtual - 5;
            this.UltimaPagina = this.PaginaAtual + 4;

            if (this.PrimeiraPagina <= 0)
            {
                this.UltimaPagina -= (this.PrimeiraPagina - 1);
                this.PrimeiraPagina = 1;
            }
            if (this.UltimaPagina > this.TotalPaginas)
            {
                this.UltimaPagina = this.TotalPaginas;
                if (this.UltimaPagina > 10)
                {
                    this.PrimeiraPagina = this.UltimaPagina - 9;
                }
            }
        }

        public int PaginaAtual { get; private set; }

        public int TamanhoPaginacao { get; private set; }

        public int TotalRegistros { get; private set; }

        public int TotalPaginas { get; private set; }

        public int UltimaPagina { get; set; }

        public int PrimeiraPagina { get; set; }

        public int Proxima
        {
            get { return (PaginaAtual + 1); }
        }
        public int Anterior
        {
            get { return (PaginaAtual - 1); }
        }

        public bool TemPaginas
        {
            get { return (TotalPaginas > 1); }
        }

        public bool TemProxima
        {
            get { return (PaginaAtual < TotalPaginas); }
        }

        public bool TemAnterior
        {
            get { return (PaginaAtual > 0); }
        }
    }
}
