﻿using System.Configuration;
using System.Reflection;

namespace Magis.Domain
{
    public class EcommerceEnvironment
    {
        public static string AssemblyVersion
        {
            get { return Assembly.GetExecutingAssembly().GetName().Version.ToString(); }
        }

        public static string Copyright
        {
            get { return "© Direitos Reservados Leanwork.com.br"; }
        }

        public static string Plataform
        {
            get { return "LeanCommerce"; }
        }

        public static string GetAppSettings(string chave, bool obrigatorio = true)
        {
            var valor = ConfigurationManager.AppSettings[chave];

            if (string.IsNullOrWhiteSpace(valor) && obrigatorio)
            {
                throw new SettingsPropertyNotFoundException(string.Format(@"Chave configuração ""{0}"" não encontrada.", chave));
            }
            return valor;
        }
    }
}