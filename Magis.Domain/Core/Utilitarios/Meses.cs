﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magis
{
    public static class Meses
    {
        public static Dictionary<int, string> Todos
        {
            get { return _todos ?? (_todos = Inicializar()); }
            set { _todos = value; }
        }
        static Dictionary<int, string> _todos;

        private static Dictionary<int, string> Inicializar()
        {
            var meses = new Dictionary<int, string>();
            meses.Add(1, "JANEIRO");
            meses.Add(2, "FEVEREIRO");
            meses.Add(3, "MARÇO");
            meses.Add(4, "ABRIL");
            meses.Add(5, "MAIO");
            meses.Add(6, "JUNHO");
            meses.Add(7, "JULHO");
            meses.Add(8, "AGOSTO");
            meses.Add(9, "SETEMBRO");
            meses.Add(10, "OUTUBRO");
            meses.Add(11, "NOVEMBRO");
            meses.Add(12, "DEZEMBRO");
            return meses;
        }
    }
}
