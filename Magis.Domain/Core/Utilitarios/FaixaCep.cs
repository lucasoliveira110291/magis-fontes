﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Magis.Domain.Core.Utilitarios
{
    public static class FaixaCep
    {
        public static IEnumerable<FaixaCepRegiao> Todas
        {
            get { return _todas ?? (_todas = Inicializar()); }
            set { _todas = value; }
        }
        static IEnumerable<FaixaCepRegiao> _todas;

        static IEnumerable<FaixaCepRegiao> Inicializar()
        {
            yield return new FaixaCepRegiao { Regiao = "SUDESTE", UF = "SP", Estado = "São Paulo", CepInicial = 1000000, CepFinal = 19999999 };
            yield return new FaixaCepRegiao { Regiao = "SUDESTE", UF = "RJ", Estado = "Rio de Janeiro", CepInicial = 20000000, CepFinal = 28999999 };
            yield return new FaixaCepRegiao { Regiao = "SUDESTE", UF = "ES", Estado = "Espírito Santo", CepInicial = 29000000, CepFinal = 29999999 };
            yield return new FaixaCepRegiao { Regiao = "SUDESTE", UF = "MG", Estado = "Minas Gerais", CepInicial = 30000000, CepFinal = 39999999 };
            yield return new FaixaCepRegiao { Regiao = "NORDESTE", UF = "BA", Estado = "Bahia", CepInicial = 40000000, CepFinal = 48999999 };
            yield return new FaixaCepRegiao { Regiao = "NORDESTE", UF = "SE", Estado = "Sergipe", CepInicial = 49000000, CepFinal = 49999999 };
            yield return new FaixaCepRegiao { Regiao = "NORDESTE", UF = "PE", Estado = "Pernambuco", CepInicial = 50000000, CepFinal = 56999999 };
            yield return new FaixaCepRegiao { Regiao = "NORDESTE", UF = "AL", Estado = "Alagoas", CepInicial = 57000000, CepFinal = 57999999 };
            yield return new FaixaCepRegiao { Regiao = "NORDESTE", UF = "PB", Estado = "Paraíba", CepInicial = 58000000, CepFinal = 58999999 };
            yield return new FaixaCepRegiao { Regiao = "NORDESTE", UF = "RN", Estado = "Rio Grande do Norte", CepInicial = 59000000, CepFinal = 59999999 };
            yield return new FaixaCepRegiao { Regiao = "NORDESTE", UF = "CE", Estado = "Ceará", CepInicial = 60000000, CepFinal = 63999999 };
            yield return new FaixaCepRegiao { Regiao = "NORDESTE", UF = "PI", Estado = "Piauí", CepInicial = 64000000, CepFinal = 64999999 };
            yield return new FaixaCepRegiao { Regiao = "NORDESTE", UF = "MA", Estado = "Maranhão", CepInicial = 65000000, CepFinal = 65999999 };
            yield return new FaixaCepRegiao { Regiao = "NORTE", UF = "PA", Estado = "Pará", CepInicial = 66000000, CepFinal = 68899999 };
            yield return new FaixaCepRegiao { Regiao = "NORTE", UF = "AP", Estado = "Amapá", CepInicial = 68900000, CepFinal = 68999999 };
            yield return new FaixaCepRegiao { Regiao = "NORTE", UF = "AM", Estado = "Amazonas", CepInicial = 69000000, CepFinal = 69299999 };
            yield return new FaixaCepRegiao { Regiao = "NORTE", UF = "RR", Estado = "Roraíma", CepInicial = 69300000, CepFinal = 69399999 };
            yield return new FaixaCepRegiao { Regiao = "NORTE", UF = "AM", Estado = "Amazonas", CepInicial = 69400000, CepFinal = 69899999 };
            yield return new FaixaCepRegiao { Regiao = "NORTE", UF = "AC", Estado = "Acre", CepInicial = 69900000, CepFinal = 69999999 };
            yield return new FaixaCepRegiao { Regiao = "CENTRO OESTE", UF = "DF", Estado = "Distrito Federal", CepInicial = 70000000, CepFinal = 72799999 };
            yield return new FaixaCepRegiao { Regiao = "CENTRO OESTE", UF = "GO", Estado = "Goiás", CepInicial = 72800000, CepFinal = 72999999 };
            yield return new FaixaCepRegiao { Regiao = "CENTRO OESTE", UF = "DF", Estado = "Distrito Federal", CepInicial = 73000000, CepFinal = 73699999 };
            yield return new FaixaCepRegiao { Regiao = "CENTRO OESTE", UF = "GO", Estado = "Goiás", CepInicial = 73700000, CepFinal = 76799999 };
            yield return new FaixaCepRegiao { Regiao = "NORTE", UF = "RO", Estado = "Roraíma", CepInicial = 76800000, CepFinal = 76999999 };
            yield return new FaixaCepRegiao { Regiao = "NORTE", UF = "TO", Estado = "Tocantins", CepInicial = 77000000, CepFinal = 77999999 };
            yield return new FaixaCepRegiao { Regiao = "CENTRO OESTE", UF = "MT", Estado = "Mato Grosso", CepInicial = 78000000, CepFinal = 78899999 };
            yield return new FaixaCepRegiao { Regiao = "CENTRO OESTE", UF = "MS", Estado = "Mato Grosso do Sul", CepInicial = 79000000, CepFinal = 79999999 };
            yield return new FaixaCepRegiao { Regiao = "SUL", UF = "PR", Estado = "Paraná", CepInicial = 80000000, CepFinal = 87999999 };
            yield return new FaixaCepRegiao { Regiao = "SUL", UF = "SC", Estado = "Santa Catarina", CepInicial = 88000000, CepFinal = 89999999 };
            yield return new FaixaCepRegiao { Regiao = "SUL", UF = "RS", Estado = "Rio Grande do Sul", CepInicial = 90000000, CepFinal = 99999999 };      
        }

        public static IEnumerable<string> ObterRegioes()
        {
            return Todas
                .GroupBy(x => x.Regiao)
                .Select(x => x.Key)
                .OrderBy(x => x);
        }

        public static IDictionary<string, string> ObterEstados()
        {
            return Todas
                .GroupBy(x => x.UF)
                .OrderBy(x => x.Key)
                .ToDictionary(x => x.Key, x => x.FirstOrDefault().Estado, StringComparer.OrdinalIgnoreCase);                
        }

        public static IEnumerable<FaixaCepRegiao> ObterFaixasPorRegiao(string regiao)
        {
            return Todas
                .Where(x => x.Regiao == regiao)                
                .OrderBy(x => x.CepInicial);
        }

        public static IEnumerable<FaixaCepRegiao> ObterFaixasPorEstado(string uf)
        {
            return Todas
                .Where(x => x.UF == uf)
                .OrderBy(x => x.CepInicial);
        }


        public class FaixaCepRegiao
        {
            public string Regiao { get; set; }
            public string Estado { get; set; }
            public string UF { get; set; }
            public int CepInicial { get; set; }
            public int CepFinal { get; set; }
        }
    }
}
