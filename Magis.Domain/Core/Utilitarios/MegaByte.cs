﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magis.Domain.Core.Utilitarios
{
    public class MegaByte
    {
        public const int Um = 1024 * 1024;
        public const int Cinco = (Um * 5);
    }
}
