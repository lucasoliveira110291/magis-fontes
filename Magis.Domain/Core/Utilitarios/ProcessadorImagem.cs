﻿using ImageResizer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Magis.Domain
{
    public class ProcessadorImagem
    {
        public int Width { get; set; }

        public int Height { get; set; }

        string _format;
        public string Format
        {
            get { return _format ?? "jpg"; }
            set { _format = value; }
        }

        int _quality;
        public int Quality
        {
            get { return (_quality == 0) ? 100 : _quality; }
            set { _quality = value; }
        }

        public ProcessadorImagem(int width = 0, int height = 0, string format = null, int quality = 0)
        {
            this.Width = width;
            this.Height = height;
            this.Format = format;
            this.Quality = quality;
        }

        public ProcessadorImagem(ImagemProduto imagem)
        {
            this.Width = imagem.Width;
            this.Height = imagem.Height;
            this.Quality = imagem.Quality;
        }

        public string ConstruirConfiguracoes()
        {
            var config = new HashSet<string>();

            if (this.Width > 0)
            {
                config.Add(String.Concat("width=", this.Width));
            }
            if (this.Height > 0)
            {
                config.Add(String.Concat("height=", this.Height));
            }
            config.Add(String.Concat("format=", this.Format));
            config.Add(String.Concat("quality=", this.Quality));
            config.Add("crop=auto");

            return String.Join("&", config);
        }

        public Stream Processar(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            if (stream.Position > 0)
            {
                //set position read to begin
                stream.Seek(0, SeekOrigin.Begin);
            }

            Stream destination = new MemoryStream();

            var imageJob = new ImageJob();
            imageJob.Source = stream;
            imageJob.Dest = destination;
            imageJob.Instructions = new Instructions(this.ConstruirConfiguracoes());
            imageJob.DisposeSourceObject = false;
             imageJob.AddFileExtension = true;
            imageJob.ResetSourceStream = true;
            ImageBuilder.Current.Build(imageJob);

            return destination;
        }
    }


    public abstract class ImagemProduto
    {
        public int Height { get; } = 0;
        public int Quality { get; } = 88;
        public abstract int Width { get; }
    }

    public class ImagemThumb : ImagemProduto
    {
        public override int Width => 100;
    }

    public class ImagemPequena : ImagemProduto
    {
        public override int Width => 300;
    }

    public class ImagemMedia : ImagemProduto
    {
        public override int Width => 600;
    }

    public class ImagemGrande : ImagemProduto
    {
        public override int Width => 900;
    }
}
