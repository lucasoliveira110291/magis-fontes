﻿using System;
using System.Data;

namespace Magis.Domain.Repositorios
{
    public interface IDbContext : IDisposable
    {
        IDbConnection CreateConnection();
    }
}
