﻿using System;
using System.Configuration;
using System.Web;

namespace Magis.Domain.Repositorios
{
    public static class ConnectionStringSettings
    {
        public static string MSSQL
        {
            get { return Obter("MSSQL"); }
        }

        public static string MONGODB
        {
            get { return Obter("MONGODB"); }
        }

        private static string Obter(string chave)
        {
            var conn = ConfigurationManager.ConnectionStrings[chave];
            if (conn == null)
            {
                throw new SettingsPropertyNotFoundException(String.Format(@"Chave configuração ""{0}"" não encontrada.", chave));
            }
            return conn.ConnectionString;
        }
    }    
}
