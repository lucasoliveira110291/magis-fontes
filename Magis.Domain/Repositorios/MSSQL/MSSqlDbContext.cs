﻿using System.Data;
using System.Data.SqlClient;

namespace Magis.Domain.Repositorios.MSSQL
{
    public class MSSqlDbContext : IDbContext
    {
        private readonly string _connectionString;

        // Default ctor.
        public MSSqlDbContext(string connString)
        {
            _connectionString = connString;
        }

        public IDbConnection CreateConnection() => new SqlConnection(_connectionString);

        public void Dispose()
        {
        }
    }
}
