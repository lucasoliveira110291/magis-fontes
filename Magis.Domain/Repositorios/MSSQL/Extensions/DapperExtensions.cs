﻿using Dapper;
using Leanwork.CodePack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Magis.Domain.Repositorios.MSSQL.Extensions
{
    public static class DapperExtensions
    {
        public static IEnumerable<IDictionary> QueryEx(
            this IDbConnection conn, 
            string sql, 
            object argSet = null, 
            string formatDateTime = "")
        {
            var result = conn
                .Query(sql, argSet) as IEnumerable<IDictionary<string, object>>;

            if (!formatDateTime.IsNullOrWhiteSpace())
            {
                return result
                    .Select(r => r.Distinct().ToDictionary(d => d.Key, d => DateTimeFormat(d.Value, formatDateTime)));
            }

            return result
                .Select(r => r.Distinct().ToDictionary(d => d.Key, d => d.Value));
        }

        static object DateTimeFormat(object value, string format)
        {
            if (value is DateTime)
            {
                DateTime date = (DateTime)value;

                return date.ToString(format);
            }

            return value;
        }
    }
}
