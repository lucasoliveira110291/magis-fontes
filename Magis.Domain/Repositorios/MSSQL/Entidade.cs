﻿using System;

namespace Magis.Domain.Repositorios
{
    /// <summary>
    /// Classe base para entidade de domínio
    /// </summary>
    public class Entidade
    {
        public int Id { get; set; }
        public DateTime CriadoEm { get; protected set; }
        public DateTime? AtualizadoEm { get; set; }

        public Entidade()
        {
            CriadoEm = DateTime.Now;
        }
    }
}
