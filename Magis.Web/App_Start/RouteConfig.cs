﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Magis.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);

            #region AUTENTICAÇÃO

            string[] namespaces = new string[] {
                "Magis.Web.Controllers"
            };

            routes.MapRoute(
                name: "Home.Index",
                url: "",
                defaults: new { controller = "Auth", action = "LogIn" },
                namespaces: namespaces
            );
            routes.MapRoute(
                name: "Auth.LogIn",
                url: "login",
                defaults: new { controller = "Auth", action = "LogIn" },
                namespaces: namespaces
            );
            routes.MapRoute(
                name: "Auth.LogOut",
                url: "logout",
                defaults: new { controller = "Auth", action = "LogOut" },
                namespaces: namespaces
            );

            routes.MapRoute(
                name: "SemPermissao.Index",
                url: "nao-autorizado",
                defaults: new { controller = "SemPermissao", action = "Index" },
                namespaces: namespaces
            );

            routes.MapRoute(
                name: "Inicio.Index",
                url: "inicio",
                defaults: new { controller = "Inicio", action = "Index" },
                namespaces: namespaces
            );

            #endregion
        }
    }
}
