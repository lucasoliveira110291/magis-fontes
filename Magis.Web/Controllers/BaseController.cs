﻿using Magis.Domain.DependencyInjection;
using Magis.Domain.Models.Comum;
using Magis.Domain.Models.Menu;
using Magis.Domain.Autenticacao;
using Magis.Domain.Modulos.Plataforma;
using Magis.Domain.Repositorios;
using Magis.Domain.Routing;
using log4net;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Magis.Domain.Services;

namespace Magis.Domain.Web.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        #region DEPENDÊNCIAS

        internal IDbContext DbContext
        {
            get => _dbContext ?? (_dbContext = ServiceLocator.Resolve<IDbContext>());
            set => _dbContext = value;
        }
        IDbContext _dbContext;

        internal IAutenticacao<UsuarioSessaoDTO> AutenticacaoUsuario
        {
            get { return _autenticacaoUsuario ?? (_autenticacaoUsuario = ServiceLocator.Resolve<IAutenticacao<UsuarioSessaoDTO>>()); }
            set { _autenticacaoUsuario = value; }
        }
        IAutenticacao<UsuarioSessaoDTO> _autenticacaoUsuario;

        internal UsuarioSessaoDTO UsuarioLogado
        {
            get { return _usuarioLogado ?? (_usuarioLogado = AutenticacaoUsuario.ObterAutenticado()); }
        }
        UsuarioSessaoDTO _usuarioLogado;

        internal ILog LoggerAdmin
        {
            get { return _loggerAdmin ?? (_loggerAdmin = LogManager.GetLogger("ADMIN")); }
            set { _loggerAdmin = value; }
        }
        ILog _loggerAdmin;

        internal ILog LoggerAuditoria
        {
            get { return _loggerAuditoria ?? (_loggerAuditoria = LogManager.GetLogger("AUDITORIA")); }
            set { _loggerAuditoria = value; }
        }
        ILog _loggerAuditoria;

        internal IUsuariosService UsuariosServices
        {
            get { return _usuariosServices ?? (_usuariosServices = ServiceLocator.Resolve<IUsuariosService>()); }
            set { _usuariosServices = value; }
        }
        IUsuariosService _usuariosServices;

        #endregion

        public new JsonResult Json(object data)
        {
            return base.Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Ok(string texto, string titulo = "", string redirecionar = "")
        {
            return Mensagens(new HashSet<MensagemAjax> {
                new SucessoAjax(texto, titulo)
            }, redirecionar: redirecionar);
        }

        public JsonResult Falhou(string texto, string titulo = "", string erro = "")
        {
            return Mensagens(new HashSet<MensagemAjax> {
                new ErroAjax(texto, titulo, erro)
            });
        }

        public JsonResult Falhou(ModelStateDictionary modelState)
        {
            IEnumerable<ModelError> allErrors = modelState.Values.SelectMany(v => v.Errors);

            var errors = from err in allErrors
                         select new ErroAjax(err.ErrorMessage);

            if (errors.Any())
                return Mensagens(errors);

            return Falhou("Não foi possível processar a requisição. Alguns dados são nulos ou inválidos!");
        }

        public JsonResult Aviso(string texto, string titulo = "")
        {
            return Mensagens(new HashSet<MensagemAjax> {
                new AvisoAjax(texto, titulo)
            });
        }

        public JsonResult Mensagens(IEnumerable<MensagemAjax> mensagens, string redirecionar = "")
        {
            if (string.IsNullOrWhiteSpace(redirecionar))
            {
                return RespostaAjax(new
                {
                    mensagens = mensagens
                });
            }
            else
            {
                return RespostaAjax(new
                {
                    mensagens = mensagens,
                    redirecionar = redirecionar
                });
            }
        }

        private JsonResult RespostaAjax(object dados)
        {
            return this.Json(dados);
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (filterContext.Result is ViewResult ||
                filterContext.Result is PartialViewResult)
            {
                SetViewBag();
            }
            base.OnResultExecuting(filterContext);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var rota = filterContext.RouteData.Route as AdminRoute;
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

            if (!filterContext.IsChildAction
                && rota != null)
            {
                if (!TemPermissao(controllerName))
                {
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        filterContext.HttpContext.Response.StatusCode = 401;
                        filterContext.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;

                        filterContext.Result = Json(new { type = "error", title = "Usuário não autorizado!!", text = "Sem permissão, entre em contato com o administrador!" });
                    }
                    else
                    {
                        filterContext.Result = new RedirectResult(Url.RouteUrl("SemPermissao.Index"));
                    }

                    return;
                }
            }

            if (UsuarioLogado != null)
            {
                MontarMenu();
            }

            base.OnActionExecuting(filterContext);
        }

        private void MontarMenu()
        {
            var menuModel = new MenuModel()
            {
                Admin = UsuarioLogado.IsPowerUser()
            };

            ViewBag.MenuModel = menuModel;
        }

        private void SetViewBag()
        {
            ViewBag.Usuario = UsuarioLogado;
#if DEBUG
            ViewBag.DEBUG = true;
#else
            ViewBag.DEBUG = false;
#endif
        }

        private bool TemPermissao(string operacao)
        {
            return UsuarioLogado.IsPowerUser();
        }
    }
}