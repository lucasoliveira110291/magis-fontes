﻿using Dapper;
using Magis.Web.Filters;
using Magis.Domain.Modulos.Plataforma;
using Leanwork.CodePack;
using System.Threading.Tasks;
using System.Web.Mvc;
using Magis.Models.Auth;
using Magis.Core;

namespace Magis.Domain.Web.Controllers
{
    public class AuthController : BaseController
    {
        [AllowAnonymous, AssemblyVersion]
        public ActionResult LogIn()
        {
            if (UsuarioLogado != null)
            {
                return RedirectToRoute("Inicio.Index");
            }
            
            var loginModel = new LogInModel() {
                ReturnUrl = Request.QueryString["returnUrl"]
            };

            return View(loginModel);
        }

        [HttpPost, AllowAnonymous, AssemblyVersion]
        public async Task<ActionResult> LogIn(LogInModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Usuario usuario;
            using (var conexao = DbContext.CreateConnection())
            {
                usuario = await conexao.QuerySingleOrDefaultAsync<Usuario>(@"Autenticacao_LogIn", new
                {
                    model.Matricula
                },
                commandType: System.Data.CommandType.StoredProcedure);
            }

            if (usuario == null)
            {
                ModelState.AddModelError("Login", "Usuário não localizado ou inativo");
                return View(model);
            }

            if (!usuario.VerificarSenhaValida(model.Senha))
            {
                ModelState.AddModelError("Login", "Senha Invalida");
                return View(model);
            }

            AutenticacaoUsuario.Autenticar(new UsuarioSessaoDTO
            {
                Id = usuario.Id,
                ImagemUrl = usuario.ImagemUrl,
                Nome = usuario.Nome.RemoverAcentos(),
                Email = usuario.Email,
                Admin = usuario.IdPerfil == Constants.PERFIL_ADMIN ? true : false
            }, model.LembrarMe);

            if (!model.ReturnUrl.IsNullOrWhiteSpace())
            {
                return Redirect(model.ReturnUrl);
            }

            return RedirectToRoute("Inicio.Index");
        }

        public ActionResult LogOut()
        {
            AutenticacaoUsuario.Deslogar();
            return RedirectToAction("LogIn");
        }
    }
}