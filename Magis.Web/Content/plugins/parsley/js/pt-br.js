﻿// ParsleyConfig definition if not already set
window.ParsleyConfig = window.ParsleyConfig || {};
window.ParsleyConfig.i18n = window.ParsleyConfig.i18n || {};
window.ParsleyConfig.errorsContainer = function (el) {
    var $parent = el.$element.parent();
    if ($parent.hasClass('input-group')) {
        return $parent.parent();
    }
    return $parent;
};

// Define then the messages
window.ParsleyConfig.i18n['pt-br'] = $.extend(window.ParsleyConfig.i18n['pt-br'] || {}, {
    defaultMessage: "campo inválido.",
    type: {
        email: "email inválido.",
        url: "URL inválida.",
        number: "não é número válido.",
        integer: "não é um inteiro válido.",
        digits: "campo deve conter apenas dígitos.",
        alphanum: "campo deve ser alfa numérico."
    },
    notblank: "campo não pode ficar vazio.",
    required: "campo obrigatório.",
    pattern: "campo parece estar inválido.",
    min: "campo deve ser maior ou igual a %s.",
    max: "campo deve ser menor ou igual a %s.",
    range: "campo deve estar entre %s e %s.",
    minlength: "campo deve ter %s caracteres ou mais.",
    maxlength: "campo deve ter %s caracteres ou menos.",
    length: "campo deve ter entre %s e %s caracteres.",
    mincheck: "Você deve escolher pelo menos %s opções.",
    maxcheck: "Você deve escolher %s opções ou mais",
    check: "Você deve escolher entre %s e %s opções.",
    equalto: "Este valor deveria ser igual.",
    date: "data inválida",
    cpf: "CPF inválido",
    cnpj: "CNPJ inválido"
});

// If file is loaded after Parsley main file, auto-load locale
if ('undefined' !== typeof window.ParsleyValidator) {
    window.ParsleyValidator.addCatalog('pt-br', window.ParsleyConfig.i18n['pt-br'], true);
}

window.ParsleyValidator.addValidator('cpf', function (value) {
    var cpf = value.replace(/[^\d]+/g, '');

    var r =
        /^(0{11}|1{11}|2{11}|3{11}|4{11}|5{11}|6{11}|7{11}|8{11}|9{11})$/;

    if (!cpf || cpf.length !== 11 || r.test(cpf)) {
        return false;
    }

    function validateDigit(digit) {
        var add = 0;
        var init = digit - 9;

        for (var i = 0; i < 9; i++) {
            add += parseInt(cpf.charAt(i + init)) * (i + 1);
        }

        return (add % 11) % 10 === parseInt(cpf.charAt(digit));
    }

    return validateDigit(9) && validateDigit(10);
}, 32);

window.ParsleyValidator.addValidator('cnpj', function (value) {
    var b = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2],
        c = value.replace(/[^\d]/g, ''),
        r = /^(0{14}|1{14}|2{14}|3{14}|4{14}|5{14}|6{14}|7{14}|8{14}|9{14})$/;

    if (!c || c.length !== 14 || r.test(c)) {
        return false;
    }

    c = c.split('');

    for (var i = 0, n = 0; i < 12; i++) {
        n += c[i] * b[i + 1];
    }

    n = 11 - n % 11;
    n = n >= 10 ? 0 : n;

    if (parseInt(c[12]) !== n) {
        return false;
    }

    for (i = 0, n = 0; i <= 12; i++) {
        n += c[i] * b[i];
    }

    n = 11 - n % 11;
    n = n >= 10 ? 0 : n;

    if (parseInt(c[13]) !== n) {
        return false;
    }

    return true;
}, 32);

window.ParsleyValidator.addValidator('date', function (value) {
    return /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/
        .test(value);
}, 32);