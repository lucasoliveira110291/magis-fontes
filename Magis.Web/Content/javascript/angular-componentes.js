﻿(function (window, angular) {
    'use strict';

    angular
        .module('LeanCommerce.Admin')
        .directive("fileread", FileRead)
        .directive("buscaMarcas", BuscaMarcas)
        .directive("buscaProdutos", BuscaProdutos)
        .directive("buscaSkus", BuscaSkus)
        .directive("buscaBrindes", BuscaBrindes)
        .directive("cartaoCredito", CartaoCredito);

    /*
    * FileRead
    * - Realiza a leitura do arquivo e converte em base64
    */
    function FileRead() {
        return {
            scope: {
                fileread: "="
            },
            link: function (scope, element, attributes) {
                element.bind("change", function (changeEvent) {
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        scope.$apply(function () {
                            scope.fileread = loadEvent.target.result;
                        });
                    };
                    reader.readAsDataURL(changeEvent.target.files[0]);
                });
            }
        };
    }

    /*
    Diretiva para autocomplete de marcas
    */
    BuscaMarcas.$inject = ['$timeout'];
    function BuscaMarcas($timeout) {

        function formatState(state) {
            if (!state.id) {
                return state.text;
            }

            var html = '';
            html += '<div style="float:left">';
            html += '   <img src="' + state.imagemUrl + '" style="padding-right:5px" width="80" />';
            html += '</div>';
            html += '<div>';
            html += '   <span>' + state.text + '</span>';
            html += '</div>';
            html += '<div style="clear: both;"></div>';

            var $state = $(html);
            return $state;
        }

        return {
            restrict: 'A',
            require: '?ngModel',
            scope: {
                marcaSelecionada: '&'
            },
            link: function ($scope, element, $attrs, ctrl) {
                return $timeout(function () {
                    var id = $(element).attr('id');

                    $(element)
                        .wrap('<div class="input-group select2-bootstrap-append"></div>')
                        .after('<span id="customer-search-button" class="input-group-btn"><a class="btn btn-primary" data-select2-open="' + id + '"><span class="glyphicon glyphicon-search"></span></a></span>');

                    $(element).select2({
                        theme: "bootstrap",
                        language: "pt-BR",
                        placeholder: {
                            id: '', text: 'Pesquisar marcas...'
                        },
                        allowClear: true,
                        ajax: {
                            url: "/api/filtro/marcas",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    term: params.term,
                                    page: params.page,
                                    size: 30
                                };
                            },
                            processResults: function (data, params) {
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: params.page * 30 < data.total
                                    }
                                };
                            },
                            cache: true
                        },
                        minimumInputLength: 3,
                        templateResult: formatState
                    });

                    $(element).select2("val", "");
                    $(element).on("select2:select", function (event) {
                        var marca = event.params.data;                        
                        $scope.$apply(function () {
                            $scope.marcaSelecionada({ marca: marca });
                        });
                    });
                });
            }
        };
    }

    /*
    Diretiva para autocomplete de produtos
    */
    BuscaProdutos.$inject = ['$timeout'];
    function BuscaProdutos($timeout) {
        return {
            restrict: 'A',
            require: '?ngModel',
            scope: {
                produtoSelecionado: '&',
                ativo: '=',
                categoriaId: '=',
                marcaId: '='
            },
            link: function ($scope, element, $attrs, ctrl) {
                return $timeout(function () {
                    var id = $(element).attr('id');

                    $(element)
                        .wrap('<div class="input-group select2-bootstrap-append"></div>')
                        .after('<span id="customer-search-button" class="input-group-btn"><a class="btn btn-primary" data-select2-open="' + id + '"><span class="glyphicon glyphicon-search"></span></a></span>');

                    function formatState(state) {
                        if (!state.id) {
                            return state.text;
                        }

                        var html = '';
                        html += '<div style="float:left">';
                        html += '   <img src="' + state.imagemUrl + '" style="padding-right:5px" width="80" />';
                        html += '</div>';
                        html += '<div>';
                        if (state.precoSobConsulta) {
                            html += '   <span>' + state.text + '<br><strong>Preço sob consulta</strong> <br> <strong>DISPONIBILIDADE: ' + state.disponivel + '</strong></span>';
                        } else {
                            html += '   <span>' + state.text + '<br><strong>' + state.preco + '</strong> <br> <strong>DISPONIBILIDADE: ' + state.disponivel + '</strong></span>';
                        }
                        html += '</div>';
                        html += '<div style="clear: both;"></div>';

                        var $state = $(html);

                        return $state;
                    }

                    $(element).select2({
                        theme: "bootstrap",
                        language: "pt-BR",
                        placeholder: {
                            id: '', text: 'Pesquisar produtos...'
                        },
                        allowClear: true,
                        ajax: {
                            url: "/api/filtro/produtos",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    term: params.term,
                                    page: params.page,
                                    size: 30,
                                    ativo: $scope.ativo,    
                                    categoriaId: $scope.categoriaId,
                                    marcaId: $scope.marcaId                                    
                                };
                            },
                            processResults: function (data, params) {                                
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: params.page * 30 < data.total
                                    }
                                };
                            },
                            cache: true
                        },
                        minimumInputLength: 3,
                        templateResult: formatState
                    });


                    $(element).select2("val", "");
                    $(element).on("select2:select", function (event) {
                        var produto = event.params.data;
                        $scope.$apply(function () {
                            $scope.produtoSelecionado({ produto: produto });
                        });
                    });
                });
            }
        };
    }

    /*
    Diretiva para autocomplete de produtos (flatten/skus)
    */
    BuscaSkus.$inject = ['$timeout'];
    function BuscaSkus($timeout) {
        return {
            restrict: 'A',
            require: '?ngModel',
            scope: {
                produtoSelecionado: '&'
            },
            link: function ($scope, element, $attrs, ctrl) {
                return $timeout(function () {
                    var id = $(element).attr('id');

                    $(element)
                        .wrap('<div class="input-group select2-bootstrap-append"></div>')
                        .after('<span id="customer-search-button" class="input-group-btn"><a class="btn btn-primary" data-select2-open="' + id + '"><span class="glyphicon glyphicon-search"></span></a></span>');

                    function formatState(state) {
                        if (!state.id) {
                            return state.text;
                        }


                        var html = '';
                        html += '<div style="float:left">';
                        html += '   <img src="' + state.imagemUrl + '" style="padding-right:5px" width="80" />';
                        html += '</div>';
                        html += '<div>';
                        html += '   <span>' + state.text + '<br><strong>' + state.preco + '</strong> <br> <strong>DISPONIBILIDADE: ' + state.disponivel + '</strong></span>';
                        html += '</div>';
                        html += '<div style="clear: both;"></div>';

                        var $state = $(html);
                        return $state;
                    }

                    $(element).select2({
                        theme: "bootstrap",
                        language: "pt-BR",
                        placeholder: {
                            id: '', text: 'Pesquisar produtos...'
                        },
                        allowClear: true,
                        ajax: {
                            url: "/api/filtro/skus",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    term: params.term,
                                    page: params.page,
                                    size: 30
                                };
                            },
                            processResults: function (data, params) {
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: params.page * 30 < data.total
                                    }
                                };
                            },
                            cache: true
                        },
                        minimumInputLength: 3,
                        templateResult: formatState
                    });


                    $(element).select2("val", "");
                    $(element).on("select2:select", function (event) {
                        var produto = event.params.data;
                        $scope.$apply(function () {
                            $scope.produtoSelecionado({ produto: produto });
                        });
                    });
                });
            }
        };
    }

    /*
    Diretiva para autocomplete de produtos brindes (flatten/skus)
    */
    BuscaBrindes.$inject = ['$timeout'];
    function BuscaBrindes($timeout) {
        return {
            restrict: 'A',
            require: '?ngModel',
            scope: {
                brindeSelecionado: '&'
            },
            link: function ($scope, element, $attrs, ctrl) {
                return $timeout(function () {
                    var id = $(element).attr('id');

                    $(element)
                        .wrap('<div class="input-group select2-bootstrap-append"></div>')
                        .after('<span id="customer-search-button" class="input-group-btn"><a class="btn btn-primary" data-select2-open="' + id + '"><span class="glyphicon glyphicon-search"></span></a></span>');

                    function formatState(state) {
                        if (!state.id) {
                            return state.text;
                        }

                        var html = '';
                        html += '<div style="float:left">';
                        html += '   <img src="' + state.imagemUrl + '" style="padding-right:5px" width="80" />';
                        html += '</div>';
                        html += '<div>';
                        html += '   <span>' + state.text + '<br><strong>' + state.preco + '</strong> <br> <strong>DISPONIBILIDADE: ' + state.disponivel + '</strong></span>';
                        html += '</div>';
                        html += '<div style="clear: both;"></div>';

                        var $state = $(html);
                        return $state;
                    }

                    $(element).select2({
                        theme: "bootstrap",
                        language: "pt-BR",
                        placeholder: {
                            id: '', text: 'Pesquisar produtos...'
                        },
                        allowClear: true,
                        ajax: {
                            url: "/api/filtro/skus/brindes",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    term: params.term,
                                    page: params.page,
                                    size: 30
                                };
                            },
                            processResults: function (data, params) {
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: params.page * 30 < data.total
                                    }
                                };
                            },
                            cache: true
                        },
                        minimumInputLength: 3,
                        templateResult: formatState
                    });


                    $(element).select2("val", "");
                    $(element).on("select2:select", function (event) {
                        var produto = event.params.data;
                        $scope.$apply(function () {
                            $scope.brindeSelecionado({ produto: produto });
                        });
                    });
                });
            }
        };
    }
    
    /*
     * CartaoCredito
     * - 
     */
    function CartaoCredito() {
        return {
            require: 'ngModel',
            restrict: 'A',
            scope: {
                bandeira: '=bandeira',
                icone: '=icone',
                model: '=ngModel',
                codigoVerificador: '=codigoVerificador'
            },
            link: function (scope, ele, attrs, ctrl) {
                scope.validacaoCartao = [
                    {
                        nome: 'ELO',
                        icone: 'icon-elo',
                        digitos: 16,
                        seguranca: 3,
                        exp: /^401178|^401179|^431274|^438935|^451416|^457393|^457631|^457632|^504175|^627780|^636297|^636368|^(506699|5067[0-6]\d|50677[0-8])|^(50900\d|5090[1-9]\d|509[1-9]\d{2})|^65003[1-3]|^(65003[5-9]|65004\d|65005[0-1])|^(65040[5-9]|6504[1-3]\d)|^(65048[5-9]|65049\d|6505[0-2]\d|65053[0-8])|^(65054[1-9]|6505[5-8]\d|65059[0-8])|^(65070\d|65071[0-8])|^65072[0-7]|^(65090[1-9]|65091\d|650920)|^(65165[2-9]|6516[6-7]\d)|^(65500\d|65501\d)|^(65502[1-9]|6550[3-4]\d|65505[0-8])/
                    },
                    {
                        nome: 'VISA',
                        icone: 'icon-visa',
                        digitos: 16,
                        seguranca: 3,
                        exp: /^4/
                    },
                    {
                        nome: 'MASTERCARD',
                        icone: 'icon-mastercard',
                        digitos: 16,
                        seguranca: 3,
                        exp: /^5[1-5]/
                    },
                    {
                        nome: 'AMEX',
                        icone: 'icon-express',
                        digitos: 15,
                        seguranca: 4,
                        exp: /^3[47]/
                    },
                    {
                        nome: 'HIPERCARD',
                        icone: 'icon-hipercard',
                        digitos: 19,
                        seguranca: 3,
                        exp: /^384/,
                        skipLuhn: true
                    },
                    {
                        nome: 'DINERS',
                        icone: 'icon-diners',
                        digitos: 14,
                        seguranca: 3,
                        exp: /^3(?:0[0-5]|[68][0-9])/
                    },
                    {
                        nome: 'HIPERCARD',
                        icone: 'icon-hipercard',
                        digitos: 16,
                        seguranca: 3,
                        exp: /^60/,
                        skipLuhn: true
                    }
                ];

                scope.computeChecksum = function (cardNo) {
                    var checksum = 0;
                    var factor = 1;
                    var temp;
                    for (var i = cardNo.length - 1; i >= 0; i--) {
                        temp = Number(cardNo.charAt(i)) * factor;
                        if (temp > 9) {
                            checksum += 1;
                            temp -= 10;
                        }
                        checksum += temp;
                        factor = factor === 1 ? 2 : 1;
                    }

                    if (checksum % 10 === 0) {
                        return true;
                    }

                    return false;
                };

                scope.getCardType = function (n) {
                    scope.bandeira = null;
                    scope.icone = null;
                    for (var i = 0; i < scope.validacaoCartao.length; i++) {
                        if (scope.validacaoCartao[i].exp.test(n)) {
                            scope.bandeira = scope.validacaoCartao[i].nome;
                            scope.icone = scope.validacaoCartao[i].icone;
                            ele.attr('maxlength', scope.validacaoCartao[i].digitos);

                            //if (scope.bandeira == "AMEX")
                            //    scope.codigoVerificador = '4';
                            //else
                            //    scope.codigoVerificador = '3';

                            return scope.validacaoCartao[i];
                        }
                    }
                    return null;
                };

                scope.validaCartao = function (numero) {
                    var cardRules = scope.getCardType(numero);
                    if (!!numero && !!cardRules && numero.toString().length === cardRules.digitos && cardRules.exp.test(numero)) {
                        return cardRules.skipLuhn || scope.computeChecksum(numero);
                    }

                    return false;
                };

                ctrl.$parsers.push(function () {
                    ctrl.$setValidity('cartaoInvalido', scope.validaCartao(ctrl.$viewValue));
                    ctrl.$setValidity('required', ctrl.$viewValue);
                    return ctrl.$viewValue;
                });
            }
        };
    }

})(window, angular);
