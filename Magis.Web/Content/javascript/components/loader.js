/* ========================================================================
 * loader.js 
 * Componente loader.
 * ======================================================================== */
'use strict';

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define([
            'loader'
        ], factory);
    } else {
        factory();
    }
}(function () {

    $(function () {

        /**
	    * Loader used to display loading inside an element.
	    */
        var Loader = function (selector) {
            this.selector = selector || 'body';
            this.loaderElement = $('<div/>', {
                'class': 'loader',
                'html': '<img width="60" height="60" src="/content/image/loading/spinner-loader.gif">'
            })
        };

        /**
         * Loader.show();
         * Show the loader and append it to the selector
         */
        Loader.prototype.show = function () {
            $(this.loaderElement).remove();
            $(this.loaderElement)
                .appendTo(this.selector)
                .fadeIn(100);
            $(this.selector).addClass('modal-open in');
        };

        /**
         * Loader.remove();
         * Hide the loader and remove it from the screen.
         */
        Loader.prototype.remove = function () {
            $(this.loaderElement).fadeOut(100, function () {
                $(this.loaderElement).remove();
            }.bind(this));
            $(this.selector).removeClass('modal-open in');
        };

        // Attach do window global object
        window['Loader'] = new Loader();
    });

}));