﻿(function () {
    "use strict";

    ParsleyValidator.addValidator('protocolo', validarProtocolo, 32)
        .addMessage("pt-br", "protocolo", "Informe o protocolo (HTTP ou HTTPS)");

    function validarProtocolo(value, requirement) {
        var upper = value.toUpperCase();

        return upper.startsWith("HTTP") || upper.startsWith("HTTPS")
    }
}());