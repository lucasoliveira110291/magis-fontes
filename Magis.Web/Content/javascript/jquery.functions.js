﻿(function ($) {
	'use strict';

	jQuery.fn.extend({
	    
		moveTo: function (options) {
			var params = $.extend({}, {
				"duration": 800
			}, options || {});

			var $element = $(this);

			setTimeout(function () {
				$('html, body').animate({
					scrollTop: $element.offset().top
				}, params);
			}, 100);

			return this;
		},

		isVisible: function(){
		    var win = $(window);
		    var viewport = {
		        top : win.scrollTop(),
		        left : win.scrollLeft()
		    };
		    viewport.right = viewport.left + win.width();
		    viewport.bottom = viewport.top + win.height();
      
		    var bounds = this.offset();
		    if (!bounds) {
		        return false;
		    }
		    bounds.right = bounds.left + this.outerWidth();
		    bounds.bottom = bounds.top + this.outerHeight();
      
		    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
		}

	});

})(jQuery);