/* ========================================================================
 * App.js v1.3.0
 * Copyright 2014 pampersdry
 * ======================================================================== */
(function () {
    'use strict';

    var APP = {
        // Core init
        // NOTE: init at html element
        // ================================
        init: function () {
            $('html').Core({
                loader: false,
                console: false
            });
        },

        // Template sidebar sparklines
        // NOTE: require sparkline plugin
        // ================================
        sidebarSparklines: {
            init: function () {
                $('aside .sidebar-sparklines').sparkline('html', { enableTagOptions: true });
            }
        },

        // Template header dropdown
        // ================================
        headerDropdown: {
            init: function (options) {
                // core dropdown function
                function coreDropdown (e) {
                    // define variable
                    var $target         = $(e.target),
                        $mediaList      = $target.find('.media-list'),
                        $indicator      = $target.find('.indicator');

                    // show indicator
                    $indicator
                        .addClass('animation animating fadeInDown')
                        .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                            $(this).removeClass('animation animating fadeInDown');
                        });

                    // Check for content via ajax
                    $.ajax({
                        url: options.url,
                        cache: false,
                        type: 'POST',
                        dataType: 'json'
                    }).done(function (data) {
                        // define some variable
                        var template    = $target.find('.mustache-template').html(),
                            rendered    = Mustache.render(template, data);

                        // hide indicator
                        $indicator.addClass('hide');

                        // update data total
                        $target.find('.count').html('('+data.data.length+')');

                        // render mustache template
                        $mediaList.prepend(rendered);

                        // add some intro animation
                        $mediaList.find('.media.new').each(function () {
                            $(this)
                                .addClass('animation animating flipInX')
                                .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                                    $(this).removeClass('animation animating flipInX');
                                });
                        });
                    });
                }

                // the dropdown
                $(options.dropdown).one('shown.bs.dropdown', coreDropdown);
            }
        },
        
        behavior: {
            init: function () {

                if ($.fn.magnificPopup) {
                    $('body').magnificPopup({
                        delegate: '.magnific',
                        type: 'image',
                        gallery: {
                            enabled: true,
                            navigateByImgClick: true,
                            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                        },
                    });
                }
                
                if ($.fn.dataTable) {
                    $.fn.dataTable.ext.errMode = 'none';// Disable datatables alert errors
                }
            }
        },

        formFields: {
            init: function () {                
                if ($.fn.numeric) {
                    setTimeout(function () {
                        $('.numeric').numeric();
                    }, 100);
                }

                if ($.fn.priceFormat) {
                    $('.money').priceFormat({
                        prefix: '',
                        centsSeparator: ',',
                        thousandsSeparator: '.',
                        centsLimit: 2
                    });

                    $('.weight, .money-precision').priceFormat({
                        prefix: '',
                        centsSeparator: ',',
                        thousandsSeparator: '.',
                        centsLimit: 3
                    });
                }

                if ($.fn.select2) {
                    $('.select2-basic').select2();
                    $('.select2-multiple').select2();
                    $.fn.select2.defaults.set("theme", "bootstrap");
                }

                if ($.fn.datepicker) {
                    $('.datepicker').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                }

                if ($.fn.datetimepicker) {
                    $('.datetimepicker').datetimepicker({
                        timeFormat: 'HH:mm',
                        changeMonth: true,
                        changeYear: true
                    }).keyup(function (e) {
                        if (e.keyCode == 8 || e.keyCode == 46) {
                            $(this).datepicker('setDate', null);
                        }
                    });
                }

                if ($.fn.multiselect) {
                    $('.multiple').multiselect({
                        "nonSelectedText": '- SELECIONE -',
                        "selectAllText": 'SELECIONAR TODOS',
                        "nSelectedText": 'SELECIONADO(S)',
                        "nonSelectedText": 'NENHUM SELECIONADO',
                        "allSelectedText": 'TODOS SELECIONADOS'
                    });
                }

                if ($.fn.tooltip) {
                    $('[data-toggle="tooltip"]').tooltip();
                }

                if ($.fn.popover) {
                    $('[data-toggle="popover"]').popover()
                }
                
                if ($.fn.parsley) {
                    $('.form-parsley').parsley();
                }
                
            }
        },

        pesquisarComEnter:{
            init: function () {
                $("body").keypress(function (e) {
                    if (e.keyCode == 13) {
                        var button = $('button[id$="-pesquisar"]');
                        if (button.length > 0) {
                            $(button[0]).trigger("click");
                        }
                    }
                });
            }
        },

        ajaxBehavior: {
            init: function () {
                // Global jQuery ajax
                $(document).bind("ajaxSend", function () {
                    if (typeof Loader === "object")
                        Loader.show();
                }).bind("ajaxStop", function () {
                    if (typeof Loader === "object")
                        Loader.remove();
                });
            }
        }
    };

    $(function () {
        // Init template core
        APP.init();
        // Init behavior
        APP.behavior.init();
        // Init form fields
        APP.formFields.init();
        // Pesquisar com ENTER
        APP.pesquisarComEnter.init();
        // Init ajax behavior
        APP.ajaxBehavior.init();

        DatePickerConfig.Init();

        // Init template sidebar summary
        //APP.sidebarSparklines.init();

        // Init template message dropdown
        //APP.headerDropdown.init({
        //    'dropdown': '#header-dd-message',
        //    'url': '../api/message.php'
        //});

        // Init template notification dropdown
        //APP.headerDropdown.init({
        //    'dropdown': '#header-dd-notification',
        //    'url': '../api/notification.php'
        //});
    });
    
})();

var DatePickerConfig = {
    Init: function () {
        $('[data-datepicker]').datepicker({
            closeText: "Fechar",
            prevText: "&#x3C;Anterior",
            nextText: "Pr�ximo&#x3E;",
            currentText: "Hoje",
            monthNames: ["Janeiro", "Fevereiro", "Mar&#xE7;o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
            dayNames: ["Domingo", "Segunda-feira", "Ter&#xE7;a-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "S&#xE1;bado"],
            dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "S&#xE1;b"],
            dayNamesMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "S&#xE1;b"],
            weekHeader: "Sm",
            dateFormat: "dd/mm/yy",
            firstDay: 0,
            isRTL: !1,
            showMonthAfterYear: !1,
            yearSuffix: ""
        });

        $('[datetime-picker]').datetimepicker({
            closeText: "Fechar",
            prevText: "&#x3C;Anterior",
            nextText: "Pr�ximo&#x3E;",
            currentText: "Hoje",
            monthNames: ["Janeiro", "Fevereiro", "Mar&#xE7;o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
            dayNames: ["Domingo", "Segunda-feira", "Ter&#xE7;a-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "S&#xE1;bado"],
            dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "S&#xE1;b"],
            dayNamesMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "S&#xE1;b"],
            weekHeader: "Sm",
            dateFormat: "dd/mm/yy",
            firstDay: 0,
            isRTL: !1,
            showMonthAfterYear: !1,
            yearSuffix: ""
        });
    }
};