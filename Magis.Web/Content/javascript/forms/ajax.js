(function () {
    'use strict';

    // Core function
    // ================================
    function formAjax(event) {
        event.preventDefault();

        var $form = $(this),
            data = $form.serialize(),
            type = $form.attr('method'),
            url = $form.attr('action'),
            redirectOnSave = $form.attr('data-redirect-onsave') || '',
            refreshOnSave = $form.attr('data-refresh-onsave') || false;

        if ($form.data('serializeArray') === true) {
            data = $form.serializeArray();
        }

        if ($form.parsley().validate()) {
            var jxhr = $.ajax({
                type: type,
                url: url,
                dataType: 'json',
                data: data
            });

            jxhr.always(function () {});
            jxhr.done(function (data) {
                var possuiErro = false;

                if (data.mensagens) {
                    var tamanhoMensagens = data.mensagens.length;
                    for (var i = 0; i < tamanhoMensagens; i++) {
                        var mensagem = data.mensagens[i];
                        toastr[mensagem.Tipo](mensagem.Titulo || '', mensagem.Texto);
                        if (mensagem.Tipo == 'error') {
                            possuiErro = true;
                        }
                    }
                }

                if (possuiErro == true) {
                    return;
                }

                if (redirectOnSave) {
                    setTimeout(function () {
                        window.location = redirectOnSave;
                    }, 500);
                    return;
                }

                if (refreshOnSave) {
                    setTimeout(function () {
                        window.location.reload(true);
                    }, 500);
                    return;
                }

                if (data.redirecionar) {
                    setTimeout(function () {
                        window.location = data.redirecionar;
                    }, 500);
                    return;
                }
            });

            jxhr.fail(function (data) {
                toastr.error('Caso o problema persista, entre em contato com o suporte!', 'Falha inesperada!');
            });
        }
    };

    $(function () {
        // Init form
        // ================================
        $('form[name="form-ajax"]').on('submit', formAjax);
        $('form.form-ajax').on('submit', formAjax);
    });

})();