﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Magis.Domain.Models.Comum
{
    public abstract class MensagemAjax
    {
        public string Titulo { get; set; }
        public string Texto { get; set; }
        public abstract string Tipo { get; }

        public MensagemAjax(string texto, string titulo = "")
        {
            this.Titulo = titulo;
            this.Texto = texto;
        }
    }

    public class SucessoAjax : MensagemAjax
    {
        public override string Tipo { get { return "success"; } }

        public SucessoAjax(string texto, string titulo = "")
            : base(texto, titulo)
        {
        }
    }

    public class ErroAjax : MensagemAjax
    {
        public override string Tipo { get { return "error"; } }
        public string Erro { get; set; }

        public ErroAjax(string texto, string titulo = "", string erro = "")
            : base(texto, titulo)
        {
            this.Erro = erro;
        }
    }

    public class AvisoAjax : MensagemAjax
    {
        public override string Tipo { get { return "warning"; } }

        public AvisoAjax(string texto, string titulo = "")
            : base(texto, titulo)
        {
        }
    }
}