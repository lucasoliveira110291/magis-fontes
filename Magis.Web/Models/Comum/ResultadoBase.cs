﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Magis.Models.Comum
{
    public class ResultadoBase
    {
        public Guid Id { get; set; }
        public string Codigo { get; set; }
        public string Trace { get; set; }
        public DateTime Data { get; set; }

        HashSet<string> _erros;
        public IEnumerable<string> Erros
        {
            get { return _erros; }
        }

        HashSet<string> _avisos;
        public IEnumerable<string> Avisos
        {
            get { return _avisos; }
        }

        public IEnumerable<string> TodasMensagens
        {
            get { return _erros.Union(_avisos); }
        }

        public bool Sucesso
        {
            get { return Erros.Any() == false; }
        }

        public bool ContemErros
        {
            get { return Erros.Any(); }
        }

        public bool ContemAvisos
        {
            get { return Avisos.Any(); }
        }

        public void Limpar()
        {
            _erros = new HashSet<string>();
            _avisos = new HashSet<string>();
        }

        public ResultadoBase()
        {
            Id = Guid.NewGuid();
            Codigo = "0";
# if DEBUG
            Trace = Environment.StackTrace;
# endif
            Data = DateTime.Now;
            _erros = new HashSet<string>();
            _avisos = new HashSet<string>();
        }

        public void AdicionarErro(string erro)
        {
            if (String.IsNullOrWhiteSpace(erro))
            {
                return;
            }
            if (_erros.Contains(erro) == false)
            {
                _erros.Add(erro);
            }
        }

        public void AdicionarErros(IEnumerable<string> erros)
        {
            foreach (var error in erros)
            {
                AdicionarErro(error);
            }
        }

        public void AdicionarAviso(string aviso)
        {
            if (String.IsNullOrWhiteSpace(aviso))
            {
                return;
            }
            if (_avisos.Contains(aviso) == false)
            {
                _avisos.Add(aviso);
            }
        }

        public void AdicionarAvisos(IEnumerable<string> avisos)
        {
            foreach (var warning in avisos)
            {
                AdicionarAviso(warning);
            }
        }
    }
}
