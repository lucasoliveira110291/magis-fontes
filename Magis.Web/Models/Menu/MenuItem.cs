﻿namespace Magis.Domain.Models.Menu
{
    public class MenuItem
    {
        public string Operation { get; set; }

        public string Group { get; set; }
    }
}