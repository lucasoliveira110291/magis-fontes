﻿using System.Collections.Generic;
using System.Linq;

namespace Magis.Domain.Models.Menu
{
    public class MenuModel
    {
        public bool Admin { get; set; }

        public IEnumerable<MenuItem> Items
        {
            get { return _items ?? (_items = new List<MenuItem>()); }
            set { _items = value; }
        }
        IEnumerable<MenuItem> _items;

        public MenuModel()
        {
            Items = new List<MenuItem>();
        }

        public bool HasGroup(string group)
        {
            return Admin || Items.Any(x => x.Group == group);
        }

        public bool HasOperation(string operation)
        {
            return Admin || Items.Any(x => x.Operation == operation);
        }

    }
}