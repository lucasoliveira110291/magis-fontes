﻿using System.ComponentModel.DataAnnotations;

namespace Magis.Models.Auth
{
    public class LogInModel
    {
        [Required(ErrorMessage = "Matrícula obrigatório")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Matrícula inválido")]
        [StringLength(100, ErrorMessage = "E-mail deve possuir no máximo 100 caracteres")]
        public string Matricula { get; set; }

        [Required(ErrorMessage = "Senha obrigatória")]
        [DataType(DataType.Password, ErrorMessage = "Senha inválida")]
        [StringLength(30, ErrorMessage = "Senha deve possuir no máximo 100 caracteres")]
        public string Senha { get; set; }

        public bool LembrarMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}