﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;

namespace Magis.Domain.Routing
{
    public static class RouteTableExtensions
    {
        public static IEnumerable<AdminRoute> GetAdminRoutes(this RouteCollection routeTable)
        {
            return routeTable.OfType<AdminRoute>();
        }
    }
}