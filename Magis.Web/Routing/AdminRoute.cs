﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace Magis.Domain.Routing
{
    public class AdminRoute : Route
    {
        public string Name { get; set; }

        public string Group { get; set; }

        public AdminRoute(string url, IRouteHandler routeHandler)
            : base(url, routeHandler) { }
    }

    public static class AreaRegistrationContextExtensions
    {
        public static void MapAdminRoute(this RouteCollection context, string name, string url, object defaults, string[] namespaces, string group = "")
        {
            context.MapAdminRoute(name, url, defaults, null, namespaces, group: group);
        }

        public static void MapAdminRoute(this RouteCollection context, string name, string url, object defaults, object constraints, string[] namespaces, string group = "")
        {
            if (context == null)
                throw new ArgumentNullException("context");

            if (url == null)
                throw new ArgumentNullException("url");

            var route = new AdminRoute(url, new MvcRouteHandler())
            {
                Name = name,
                Defaults = new RouteValueDictionary(defaults),
                Constraints = new RouteValueDictionary(constraints),
                DataTokens = new RouteValueDictionary(),
                Group = group
            };

            if ((namespaces != null) && (namespaces.Length > 0))
            {
                route.DataTokens["Namespaces"] = namespaces;
            }            

            if (String.IsNullOrEmpty(name))
                context.Add(route);
            else
                context.Add(name, route);
        }
    }
}