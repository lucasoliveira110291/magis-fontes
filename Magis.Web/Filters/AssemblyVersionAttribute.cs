﻿using Magis.Domain;
using System.Web.Mvc;

namespace Magis.Web.Filters
{
    public class AssemblyVersionAttribute : System.Web.Mvc.ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var viewBag = filterContext.Controller.ViewBag;
            viewBag.AssemblyVersion = EcommerceEnvironment.AssemblyVersion;
        }
    }
}